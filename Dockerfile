FROM openjdk:12-alpine

COPY target/pinpoint-message-producer-*.jar /target/pinpoint-message-producer.jar
COPY target/classes /target/classes

CMD ["java", "-jar", "/target/pinpoint-message-producer.jar"]