import com.google.gson.Gson;
import interfaces.IKafkaConstants;
import interfaces.IProducerConstants;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import pojos.PinpointMessage;
import java.util.Properties;

/**
 * Created by student on 26.04.19.
 */
public class Main
{
    public static void main( String[] args )
    {
        System.out.println( "Producer started" );

        runProducer( );

        //runConsumer( );

        //runWhileLoop( );
    }

    private static void runProducer( )
    {
        Properties props = new Properties( );
        props.put( ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, IKafkaConstants.KAFKA_BROKERS );
        props.put( ProducerConfig.ACKS_CONFIG, IProducerConstants.ACKS_CONFIG );
        props.put( ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName( ) );
        props.put( ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName( ) );

//        props.put( ProducerConfig.CLIENT_ID_CONFIG, interfaces.IKafkaConstants.CLIENT_ID );
//        props.put( ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, LongSerializer.class.getName( ) );
//        props.put( ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName( ) );

        Producer< String, String > producer = new KafkaProducer<>( props );

        while ( true )
        {
            PinpointMessage pinpointMessage = createPinpointMessage( );
            String jsonString = messageToJsonString( pinpointMessage );

            producer.send( new ProducerRecord<>( IKafkaConstants.PINPOINT_TOPIC, "pinpoint-message-producer",
                    jsonString ) );

            System.out.println(System.currentTimeMillis() + ": sendMessage");

            try
            {
                Thread.sleep( 5000 );
            }
            catch ( InterruptedException e )
            {
                e.printStackTrace( );

                producer.close( );
            }
        }
    }

    private static PinpointMessage createPinpointMessage( )
    {
        PinpointMessage pinpointMessage = new PinpointMessage( );

        pinpointMessage.setMessage( System.currentTimeMillis() + ": hallo" );

        return pinpointMessage;
    }

    private static String messageToJsonString( final PinpointMessage pinpointMessage )
    {
        Gson gson = new Gson( );

        return gson.toJson( pinpointMessage );
    }
}